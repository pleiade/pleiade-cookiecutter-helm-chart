# Pleiade cookiecutter Helm chart

[Cookiecutter](https://cookiecutter.readthedocs.io/en/stable/index.html) template for a Helm chart, self-published in GitLab pages.

## Quick Start

Generate a project with
```sh
cookiecutter https://gitlab.inria.fr/pleiade/pleiade-cookiecutter-helm-chart
```

| Fields | Default | Description |
| :---  | :---  | :---  |
| `full_name` | Somebody | Main author |
| `email` | nobody@inria.fr | Contact address for the author |
| `website` | https://team.inria.fr/pleiade | Website for the author |
| `project_name` | app | Verbose project name |
| `repo_name` | app-chart | Repository name and root directory name |
| `release_date` | 2023-01-01 | Release date of the project |
| `version` | 0.1.0 | Release version (see `.bumpversion.cfg`) |
| `license` | CeCill-B | License to use |

## Features
- Choice of licenses
- Chart lint and publishing using GitLab CI
